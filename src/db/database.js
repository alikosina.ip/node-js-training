const db = require('mongoose');

db.connect('mongodb://my-mongo:27017/test1', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // useFindAndModify: false,
    // useCreateIndex: true
})

module.exports = db