const db = require('./database');

const schema = new db.Schema(
    {
        _id: {
            type: String
        },
        name: {
            type: String
        }
    }
)

schema.methods.getName = function() {
    return this.name;
}

module.exports = db.model('Product', schema);

