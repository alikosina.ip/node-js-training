const express = require('express');
const path = require('path');
const cors = require('cors');
// const db = require('./db/database');
const User = require('./db/schemas');

const app = express();

const corsOptions = {
    origin: 'http://localhost:3000',
  }

app.use(cors(corsOptions));

const connect = async () => {
    try {
        // mongoose.connect('mongodb://localhost:27017', {
        //     useNewUrlParser: true,
        //     useUnifiedTopology: true,
        //     // useFindAndModify: false,
        //     // useCreateIndex: true
        // })

        app.listen(5000, () => {
            console.log('Server is listening on port 5000')
        })
    } catch {
        console.log('Wrong connection!')
    }
}

app.use('/styles', express.static('./assets/styles'))

app.get('/', async (req, res) => {
    const pathname = path.resolve(__dirname, 'content', 'index.html')
 const single = await User.find({
 }).exec()
 console.log('seingle  = ', single)
    res.json(single)
});



app.get('/about', (req, res) => {
    const { query } = req;
    console.log(query)
    res.send('About page')
})

app.get('*', (req, res) => {
    res.status(404);
    res.send('Not Found')
})

connect()