FROM node:14-alpine

WORKDIR /var/www/app

RUN npm install -g npm@latest

COPY package.json ./

COPY yarn.lock ./

RUN yarn install

COPY . .

EXPOSE 5000

CMD ["npm", "run", "start:express"]