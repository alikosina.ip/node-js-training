build:
	docker build -f Dockerfile . -t node-react
	docker run --rm  -it --name my-backend --network my-network -v ${PWD}:/var/www/app -p 5000:5000 node-react

mongo:
	docker run --rm -it -p 27018:27017 --network my-network --name my-mongo mongo:latest