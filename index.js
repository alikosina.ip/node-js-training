const path = require('path');
const { readFile } = require('fs');
const http = require('http');

const pathname = path.resolve(__dirname, 'content', 'test.txt');

readFile(pathname, 'utf8', (err, res) => {
    if(err) {
        console.log(err);

        return;
    }

    console.log(res);

    return;
})

const server = http.createServer((req, res) => {
    if(req.url === '/') {
        // res.write('Home');
        res.end('<h1>Home update</h1>');
        return;
    }
    res.end('Hello')
})

server.listen(5000)